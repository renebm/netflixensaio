// - GLOBALS 
//-------------------------------------------

var w = window.innerWidth;
var url 		 = 'http://'+window.location.hostname+'/nutracorpore/wp-admin/admin-ajax.php';
var url_site 	 = 'http://'+window.location.hostname+'/nutracorpore/';

//- IE FIX
//-------------------------------------------------

(function msiedetection() {
	var ie = window.navigator.userAgent.indexOf("MSIE ")
	// If Internet Explorer, return true
	if (ie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  {
		document.querySelector('html').classList.add('ie');
	}
	// If another browser, return false
	else  {
		console.log('thank you for not using ie')
	}	
})();


//- FIREFOX FIX 
//-------------------------------------------------

if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
	document.querySelector('html').classList.add('firefox');
}


//- SLICK 
//-------------------------------------------------

$('.full-bg').slick({
	arrows: false, 
	dots: true
});

$('.sales .grid-products').slick({
	arrows: false, 
	dots: false,
	infinite: true,
	mobileFirst: true,
	slidesToShow: 1, 
	responsive: [
	{
		breakpoint: 600, 
		settings: {
			slidesToShow: 3
		}
	},
	{
		breakpoint: 997, 
		settings: {
			slidesToShow: 4
		}
	},
	{
		breakpoint: 1279, 
		settings: {
			slidesToShow: 5
		}
	}
	]
});



if (w < 997) {
	$('.categories').slick({
		arrows: false, 
		dots: false, 
		mobileFirst: true, 
		slidesToShow: 1, 
		responsive: [
		{
			breakpoint: 600, 
			settings: {
				slidesToShow: 4
			}
		},
		{
			breakpoint: 997, 
			settings: 'unslick'
		}
		]
	});
}

$('.shaded-box .inner').slick({
	slidesToShow: 1, 
	dots: false,
	arrows: true,
	mobileFirst: true, 
	responsive: [
	{
		breakpoint: 600, 
		settings: {
			slidesToShow: 3
		}
	},
	{
		breakpoint: 997, 
		settings: {
			slidesToShow: 4
		}
	},
	]
}); 

$('.product-picture').slick({
	dots: false, 
	arrows: true
});

//-INDEX MENU PICKER 
//---------------------------------

$('.submenu-picker').on('click', function(){
	if (w > 600) {	
		$('.submenu-picker, .option').removeClass('active'); 
		$('.option[data-picker="'+$(this).data('picker')+'"').toggleClass('active');
	}

	else {
		window.location = $(this).data('url'); 
	}
});



//- HEADER 
//-------------------------------------------------


var headerEl  = document.getElementsByTagName('header')[0], 
isScrolled = function(){
	if(window.pageYOffset > 120) {
		headerEl.classList.add('is-scrolled');  
	}

	else {
		headerEl.classList.remove('is-scrolled');
	}
}

window.onscroll = isScrolled;  


$('.hamburger').on('click', function(){
	if($(this).hasClass('is-open')) {
		$(this).removeClass('is-open');
		$('header, .mobile').removeClass('is-open');
		window.setTimeout(function(){	
			$('.upper > li').removeClass('menu-out menu-in');
			$('.contact-information').removeClass('hidden'); 
		}, 400)
	}
	else {
		$(this).addClass('is-open');
		$('header, .mobile').addClass('is-open');
	}

});

$('.upper li').on('click', function(){
	if ($('.hamburger').hasClass('is-open')) {	
		$('.upper > li:not(".go-back")').not($(this)).addClass('menu-out');
		$('.go-back').addClass('menu-in');
		$(this).addClass('menu-in');
		$('.contact-information').addClass('hidden'); 
	}
});


$('.go-back').on('click', function(){
	$(this).removeClass('menu-in'); 
	$('.upper li').removeClass('menu-in menu-out'); 
	$('.contact-information').removeClass('hidden')
});

//- SMARTSEARCH 
//--------------------------------------------------
$('.search input').on('keyup', function(){
	if ($(this).val().length > 5) {
		
		$.ajax({
			url: url,
			data: {search: $(this).val(), action: 'searchProducts'},
			type: 'POST',
			success: function(data) {
				var response = JSON.parse(data);
				for (var r in response) {
					
				}
			}
		});

		$('.search .dropdown').css({
			'visibility' : 'visible',
			'opacity' : '1'
		});
	}
	else {
		$('.search .dropdown').attr('style', '');
	}
});



//- CART PRODUCT VALUE   
//---------------------------------------------------
$('.numeral').on('click', function() {

	var oldValue = $(this).siblings('input').val();

	if ($(this).text() == '+') {
		var newVal = parseFloat(oldValue) + 1;
	} 
	else {

		if (oldValue > 0) {
			var newVal = parseFloat(oldValue) - 1;
		} else {
			newVal = 0;
		}
	}

	if (newVal >= 10) {	
		$(this).siblings("input").val(newVal);
	}

	else {
		$(this).siblings("input").val('0'+newVal);
	}

});

$('#edit').on('click', function() {
	$('.edit-account input, .edit-account select').removeAttr('disabled');
	$(this).html('ENVIAR'); 
});


$('.add-cart').on('click', addToCart);

function addToCart() {
	var id = $(this).data('id');
	var variable = $(this).data('variable');
	var variation = $(this).data('variation');

	if (variable == 'variable' && variation == 0) {
		swal({
			type: 'error',
			title: 'Por favor, selecione uma variação'
		});

		return false;
	}

	$.ajax({
		url: url,
		data: {id: id, variation: variation, quantity: 1, action: 'addToCart'},
		type: 'POST',
		success: function(data) {
			window.location = url_site+'/carrinho';
		}
	});
}


$('#coupon-calculate').on('submit', couponCalculate);

function couponCalculate(e) {
	e.preventDefault();
	var id = $('input[name=coupon-number]').val();
	$.ajax({
		url: url,
		data: {id: id, action: 'couponCalculate'},
		type: 'POST',
		success: function(data) {
			if (data) {
				swal({
					type: 'success',
					title: 'Cupom Adicionado com sucesso'
				}, function() {
					location.reload();
				});
			} else {
				swal({
					type: 'error',
					title: 'Cupom Inválido'
				});
			}
		}
	});
}


$('.numeral').on('click', changeQuantity);

function changeQuantity()
{
	var quantity = $(this).siblings('input[name=quantity]').val();
	var id 		 = $(this).siblings('input[name=quantity]').data('id');
	
	if (quantity <= 0) {
		$(this).siblings('input[name=quantity]').val(1);
		swal({
			type: 'error',
			title: 'A Quantidade não pode ser menor que 0'
		});

		return false;
	}

	$.ajax({
		url: url,
		data: {id: id, quantity: quantity, action: 'changeQuantity'},
		type: 'POST',
		success: function(data) {
			swal({
				type: 'success',
				title: 'Quantidade alterada com sucesso'
			}, function() {
				location.reload();				
			});
		}
	});
}


$('#register').on('submit', register);

function register(e) {
	e.preventDefault();
	var data = $(this).serialize();

	$.ajax({
		url: url,
		data: data,
		type: 'POST',
		success: function(data) {
			var response = JSON.parse(data);
			if (typeof(response.success) == 'undefined') {
				$('input[name='+r+']').parents('label').find('.error-field').html('');
				for (var r in response) {
					// $('input[name='+r+']').parents('label').append('<span class="error-field">'+response[r]+'</span>');
					$('input[name='+r+']').parents('label').find('.error-field').html(response[r]);
				}

				return false;
			}
			
			//redirect to cart
		}
	});
}


$(document).ready(function() {
	// $('input[name=phone]').mask('99 9999-9999');
	$('input[name=zipcode]').mask('99999-999');

	$('input[name=cielo_credit_expiry]').mask('99/9999');

	$('input[name=cpf]').mask('999.999.999-99');
	$('input[name=person]').on('click', function() {
		$('input[name=cpf]').unmask();
		if ($(this).val() == 'fisica') {
			$('input[name=cpf]').mask('999.999.999-99');		
			$('input[name=cpf]').siblings('.average').html('CPF');
		} else {
			$('input[name=cpf]').mask('99.999.999/9999-99');
			$('input[name=cpf]').siblings('.average').html('CNPJ');
		}
	});

	$('input[name=cellphone]').on("keypress", function() {
		if ($(this).val().length <= 13) {
			$('input[name="cellphone"]').unmask();
			$('input[name="cellphone"]').mask("(99) 9999-9999");
		} else {
			$('input[name="cellphone"]').unmask();
			$('input[name="cellphone"]').mask("(99) 99999-9999");
		}
	});

	$('input[name=phone]').on("keypress", function() {
		if ($(this).val().length <= 13) {
			$('input[name="phone"]').unmask();
			$('input[name="phone"]').mask("(99) 9999-9999");
		} else {
			$('input[name="phone"]').unmask();
			$('input[name="phone"]').mask("(99) 99999-9999");
		}
	});

	$('input[name=payment_method]').on('click', function() {
		var id = $(this).val();
		$('.negative').slideUp();
		$('#'+id).slideDown();
	});

	$('#variation').on('change', function() {
		$('.add-cart').data('variation', $(this).val());
	});
});


$('#calculate').on('keyup', searchAddress);

function searchAddress() {
	var zipcode = $(this).val();
	zipcode = zipcode.replace('-', '');
	if (zipcode.length == 8) {
		$.ajax({
			url: 'https://api.pagar.me/1/zipcodes/'+zipcode,
			type: 'GET',
			success: function(response) {
				$('input[name=state]').val(response.state);
				$('input[name=city]').val(response.city);
				$('input[name=address]').val(response.street);
				$('input[name=neighborhood]').val(response.neighborhood);
				$('input[name=number]').focus();
			}
		});
	}
}


$('.newsletter').on('submit', newsletter);

function newsletter() {
	var data = $(this).serialize();
	$.ajax({
		url: url,
		data: data,
		type: 'POST',
		success: function(data) {
			swal({
				type: 'success',
				title: 'Cadastro realizado com sucesso'
			});
		}
	});
}


$('.remove').on('click', removeProduct);

function removeProduct() {
	var id = $(this).data('id');

	swal({
		title: "",
		text: "Você deseja mesmo excluir esse produto do carrinho?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Sim",
		cancelButtonText: "Não",
		closeOnConfirm: false
	},
	function(isConfirm) {
		if (isConfirm) {
			$.ajax({
				url: url,
				data: {id: id, action: 'removeProduct'},
				type: 'POST',
				success: function(data) {
					swal({
						type: 'success',
						title: 'Produto removido com sucesso'
					}, function() {
						location.reload();
					});
				}
			});
		}
	});
}


$('#login').on('submit', login);

function login(e) {
	e.preventDefault();
	var data = $(this).serialize();
	var redirect = $('input[name=redirect]').val();

	$.ajax({
		url: url,
		data: data,
		type: 'POST',
		success: function(data) {
			var response = JSON.parse(data);
			
			if (typeof(response.errors) != 'undefined') {
				swal({
					type: 'error',
					title: 'Login/Senha incorretos'
				});

				return false;
			}

			if (redirect.length > 0) {
				window.location = url_site+'/'+redirect;
			} else {
				window.location = url_site+'/minha-conta';	
			}
		}
	});
}


$('input[name=rating]').on('click', rating);

function rating() {
	var product_id = $(this).data('id');
	var rating	   = $(this).val();
	$.ajax({
		url: url,
		data: {'id': product_id, 'rating': rating, action: 'rating'},
		type: 'POST',
		success: function(data) {
			swal({
				type: 'success',
				title: 'Obrigado por sua opinião'
			});
		}
	});
}


$('#calculate').on('keyup', shippingCalculate);

function shippingCalculate() {
	var data = $(this).val();
	var verify = false;
	if (data.length == 9) {
		$('#shipping-results').slideToggle();
		$('#shipping-results').html('');
		$.ajax({
			url: url,
			data: {'cep': data, action: 'checkoutCalculate'},
			type: 'POST',
			success: function(data) {
				var methods = JSON.parse(data);
				var html = '';

				for (var method in methods) {
					html += '<label>';
					html += '	<div class="field radiobox">';
					html += '		<span class="boxed">';
					html += '			<input type="radio" name="shipping_method" value="'+methods[method].metodo+'">';
					html += '		</span>';
					html += '		<small class="freight-price">R$ '+methods[method].Valor+'</small>';
					html += '		<small class="delivery-time"><b> '+methods[method].PrazoEntrega+' Dia(s)</b></small>';
					html += '		<small class="means">'+methods[method].metodo+'</small>';
					html += '	</div>';
					html += '</label>';
				}

				$('#shipping-results').html(html);
				$('#shipping-results').slideToggle();
			}
		});
	}
}


$('#shipping-product, #cart-shipping-calculate').on('submit', shippingProduct);
var verify = false;
function shippingProduct(e) {
	e.preventDefault();
	var data = $(this).serialize();
	var zipcode = $('input[name=zipcode]').val();
	if (! verify) {
		$.ajax({
			url: url,
			data: data,
			type: 'POST',
			success: function(data) {
				var methods = JSON.parse(data);
				var html = '';
				
				for (var method in methods) {
					if (methods[method].Erro != 0) {
						swal({
							type: 'error',
							title: methods[method].MsgErro
						});
						return false;
					}
					verify = true;
					html += '<tr>';
					html += '	<td>R$ '+methods[method].Valor+'</td>';
					html += '	<td>'+methods[method].metodo+' para o CEP <br>'+zipcode+' em até '+methods[method].PrazoEntrega+' dia(s).</td>';
					html += '</tr>';
				}

				$('#shipping_totals').append(html);
			}
		});
	}
}


$('#payment_form').on('submit', payment_method);

function payment_method(e) {
	e.preventDefault();
	var data = $(this).serialize();

	$.ajax({
		url: url,
		data: data,
		type: 'POST',
		success: function(data) {
			if (data.result == 'failure') {
				swal({
					type: 'error',
					title: data.messages
				});

				return false;
			}
			document.cookie = "address-checkout=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
			var order = data.redirect.split('=');
			window.location = url_site+'/obrigado?key='+order[1];
		}
	});
}


$('#register-checkout').on('submit', registerCheckout);

function registerCheckout(e) {
	e.preventDefault();
	var data = $(this).serialize();

	$.ajax({
		url: url,
		data: data,
		type: 'POST',
		success: success
	});	

	function success(data) {
		if (! data) {
			swal({
				type: 'error',
				title: 'Ocorreu um erro ao efetuar o cadastro'
			});
		} else if(data) {
			swal({
				type: 'success',
				title: 'Cadastro efetuado com sucesso'
			}, function() {
				location.reload();
			});
		} else {
			var response = JSON.parse(data);
			if (typeof(response.success) == 'undefined') {
				$('input[name='+r+']').parents('label').find('.error-field').remove();
				for (var r in response) {
					$('input[name='+r+']').parents('label').append('<span class="error-field">'+response[r]+'</span>');
				}

				return false;
			}
		}
	}
}


$('#address-checkout').on('submit', addressCheckout);

function addressCheckout(e) {
	e.preventDefault();
	var data = $(this).serialize();
	var method = '';
	$('input[name=shipping_method]').each(function() {
		if ($(this).is(':checked')) {
			method = $(this).val();		
		}
	});

	if (method.length == 0) {
		swal({
			type: 'error',
			title: 'Por favor selecione um método de entrega'
		});
		return false;
	}

	$.ajax({
		url: url,
		data: data,
		type: 'POST',
		success: function(data) {
			if (data) {
				document.cookie = 'address-checkout='+method;
				swal({
					type: 'success',
					title: 'Cadastro efetuado com sucesso'
				}, function() {
					location.reload();
				});
			}
		}
	});

}


$('.category-select').on('click', filterProducts);

var categories = [];
function filterProducts() {
	var value = $(this).val();
	if ($(this).is(':checked')) {
		categories.push(value);	
	} else {
		delete categories[categories.indexOf(value)];
	}

	$.ajax({
		url: url,
		data: {categories: categories.join(','), action: 'filterProducts'},
		type: 'POST',
		success: function(data) {
			console.log(data);
		}
	});
}


if (!$('html').hasClass('ie')) {
	$('.product-picture img').wrap('<span style="display:inline-block"></span>')
	.css('display', 'block')
	.parent()
	.zoom();
}

$(document).ajaxStart(function() {
	$('.loader-overlay').fadeIn(); 
});

$(document).ajaxComplete(function(event, xhr, settings) {
	$('.loader-overlay').delay(2000).fadeOut();
});