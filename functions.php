<?php

// Add thumbnail support
//--------------------------
add_theme_support( 'post-thumbnails');

/* ************************
POST TYPE BANNERS
************************* */
function Banner() {
    $labels = array(

        'name' => _x('Banners', 'post type general name'),
        'singular_name' => _x('Banners', 'post type singular name'),
        'add_new' => _x('Adicionar Banner', 'Novos Banners'),
        'add_new_item' => __('Novo Banner'),
        'edit_item' => __('Editar Banner'),
        'new_item' => __('Novo Banner'),
        'view_item' => __('Ver Banner'),
        'search_items' => __('Procurar Banner'),
        'not_found' =>  __('Nenhum registro encontrado'),
        'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Banners'

        );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title','editor','thumbnail')
        );

    register_post_type( 'Banner' , $args );
}

add_action('init', 'Banner');


/* ************************
POST TYPE MARCAS
************************* */
function Marca() {
    $labels = array(

        'name' => _x('Marcas', 'post type general name'),
        'singular_name' => _x('Marcas', 'post type singular name'),
        'add_new' => _x('Adicionar Marca', 'Novos Marcas'),
        'add_new_item' => __('Novo Marca'),
        'edit_item' => __('Editar Marca'),
        'new_item' => __('Novo Marca'),
        'view_item' => __('Ver Marca'),
        'search_items' => __('Procurar Marca'),
        'not_found' =>  __('Nenhum registro encontrado'),
        'not_found_in_trash' => __('Nenhum registro encontrado na lixeira'),
        'parent_item_colon' => '',
        'menu_name' => 'Marcas'

        );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'public_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => null,
        'supports' => array('title','editor','thumbnail')
        );

    register_post_type( 'Marca' , $args );
}

add_action('init', 'Marca');



function addToCart()
{
	global $woocommerce;

	$woocommerce->cart->set_session();
    if (isset($_POST['variation']) && $_POST['variation'] > 0) {
        $pedido = $woocommerce->cart->add_to_cart($_POST["id"], $_POST['quantity'], $_POST['variation']);
    } else {
        $pedido = $woocommerce->cart->add_to_cart($_POST["id"], $_POST['quantity']);
    }
	

	$return = array(
		'success' => true,
		'cart' => $pedido
	);

	print_r(json_encode($return));
	die();
}

add_action('wp_ajax_addToCart', 'addToCart');
add_action('wp_ajax_nopriv_addToCart', 'addToCart');


function couponCalculate()
{
	global $woocommerce;
	print_r($woocommerce->cart->add_discount($_POST['id']));
    die();
}

add_action('wp_ajax_couponCalculate', 'couponCalculate');
add_action('wp_ajax_nopriv_couponCalculate', 'couponCalculate');


function login()
{
	$info = array();
	$info['user_login'] = $_POST['email'];
	$info['user_password'] = $_POST["password"];
	$info['remember'] = true;

	$user_signon = wp_signon( $info, false );

	echo json_encode($user_signon);
    die();
}

add_action('wp_ajax_login', 'login');
add_action('wp_ajax_nopriv_login', 'login');


function changeQuantity()
{
	global $woocommerce;
    $woocommerce->cart->set_quantity($_POST['id'], $_POST['quantity'], true);

    $return = array(
        'success' => true
    );

    print_r(json_encode($return));
    die();
}

add_action('wp_ajax_changeQuantity', 'changeQuantity');
add_action('wp_ajax_nopriv_changeQuantity', 'changeQuantity');


function removeProduct()
{
    global $woocommerce;
    $woocommerce->cart->remove_cart_item($_POST['id']);

    $return = array(
        'success' => true
    );

    print_r(json_encode($return));
    die();
}

add_action('wp_ajax_removeProduct', 'removeProduct');
add_action('wp_ajax_nopriv_removeProduct', 'removeProduct');


function registerCheckout() 
{
    global $wpdb, $user_ID, $woocommerce;

    $error = array();

    $name      = $_POST['name'];
    $email     = $_POST['email'];
    $cpf       = $_POST['cpf'];
    $cellphone = $_POST['cellphone'];
    $password  = $_POST['password'];
    $password_confirm  = $_POST['password-confirm'];

    if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error['email'] = 'Preencha corretamente o campo email';
    }

    if (strlen($name) == 0) {
        $error['name'] = 'Preencha corretamente o campo nome';
    }

    if (strlen($cellphone) == 0) {
        $error['cellphone'] = 'Preencha corretamente o campo Telefone';
    }

    if (strlen($cpf) == 0) {
        $error['cpf'] = 'Preencha corretamente o campo CPF';
    }

    if (strlen($password) < 6) {
        $error['password'] = 'Sua senha deve conter mais de 6 caracteres';
    }

    if (! isset($error['password']) && ($password != $password_confirm)) {
        $error['password'] = 'As senhas são incompativeis';
        $error['password-confirm'] = 'As senhas são incompativeis';
    }

    if (count($error) > 0) {
        echo json_encode($error);
        die();
    }
    
    $new_user_id = wp_create_user( $name, $password, $email );
    // print_r($new_user_id);
    // die();
    
    if (is_object($new_user_id)) {
        echo false;
        die();
    }

    add_user_meta( $new_user_id, 'cpf', $cpf);
    add_user_meta( $new_user_id, 'cellphone', $cellphone);

    $info = array(
        'user_login' => $email,
        'user_password' => $password,
        'remember' => true
    );

    $user_signon = wp_signon( $info, false );

    echo true;
    die();
}

add_action('wp_ajax_registerCheckout', 'registerCheckout');
add_action('wp_ajax_nopriv_registerCheckout', 'registerCheckout');


function addressCheckout()
{
    global $woocommerce, $current_user;

    $city = $_POST['city'];
    $state = $_POST['state'];
    $zipcode = $_POST['zipcode'];
    $address = $_POST['address'].', '.$_POST['number'];
    $number = $_POST['number'];
    $complement = $_POST['complement'];
    $reference = $_POST['reference'];
    $neighborhood = $_POST['neighborhood'];

    add_user_meta( $current_user->ID, 'complement', $complement);
    add_user_meta( $current_user->ID, 'reference', $reference);
    add_user_meta( $current_user->ID, 'neighborhood', $neighborhood);
    $woocommerce->customer->set_shipping_city($city);
    $woocommerce->customer->set_shipping_state($state);
    $woocommerce->customer->set_shipping_address($address);
    $woocommerce->customer->set_shipping_postcode($zipcode);
    $woocommerce->customer->set_shipping_country('BR');

    echo true;
    die();
}

add_action('wp_ajax_addressCheckout', 'addressCheckout');
add_action('wp_ajax_nopriv_addressCheckout', 'addressCheckout');


function register()
{
	global $wpdb, $user_ID, $woocommerce;
    $error = array();
    
    $type = $_POST['type'];
    $name = $_POST['name'];
    $cpf = $_POST['cpf'];
    $cellphone = $_POST['cellphone'];
    $phone = $_POST['phone'];
    $sex = $_POST['sex'];
    $birthday = $_POST['birthday'];
    $email = $_POST['email'];
    $confirm_email = $_POST['confirm-email'];
    $password = $_POST['password'];
    $password_confirm = $_POST['password-confirm'];
    

    if (! filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $error['email'] = 'Preencha corretamente o campo email';
    }

    if (! isset($error['email']) && ($email != $confirm_email)) {
        $error['email'] = 'Campos de email diferentes';
        $error['email-confirm'] = 'Campos de email diferentes';
    }

    if (strlen($password) < 6) {
        $error['password'] = 'Sua senha deve conter mais de 6 caracteres';
    }

    if (! isset($error['password']) && ($password != $password_confirm)) {
        $error['password'] = 'As senhas são incompativeis';
        $error['password-confirm'] = 'As senhas são incompativeis';
    }

    if (strlen($name) == 0) {
        $error['name'] = 'Preencha corretamente o campo nome';
    }

    if (strlen($cpf) == 0) {
        $error['cpf'] = 'Preencha corretamente o campo CPF';
    }

    if (strlen($cellphone) == 0) {
        $error['cellphone'] = 'Preencha corretamente o campo Celular';
    }

    if (strlen($phone) == 0) {
        $error['phone'] = 'Preencha corretamente o campo Telefone';
    }

    if ($sex == 0) {
        $error['sex'] = 'Preencha corretamente o campo Sexo';
    }

    if (strlen($birthday) == 0) {
        $error['bithday'] = 'Preencha corretamente o campo Data de Nascimento';
    }
    
    if (count($error) == 0) {
        if ($_POST["id"] == 0) {
            $new_user_id = wp_create_user( $name, $password, $email );

            add_user_meta( $new_user_id, 'tipo', $type);
            add_user_meta( $new_user_id, 'cpf', $cpf);
            add_user_meta( $new_user_id, 'birthday', $birthday);
            add_user_meta( $new_user_id, 'sex', $sex);
            add_user_meta( $new_user_id, 'billing_phone', $phone);
            add_user_meta( $new_user_id, 'cellphone', $cellphone);

            $response = array(
                'success' => true,
                'id' => $new_user_id
            );

            $info = array(
                'user_login' => $email,
                'user_password' => $password,
                'remember' => true
            );

            $user_signon = wp_signon( $info, false );

            $city = $_POST['city'];
            $state = $_POST['state'];
            $zipcode = $_POST['zipcode'];
            $address = $_POST['address'];
            $number = $_POST['number'];
            $complement = $_POST['complement'];
            $reference = $_POST['reference'];

            add_user_meta( $new_user_id, 'number', $number);
            add_user_meta( $new_user_id, 'complement', $complement);
            add_user_meta( $new_user_id, 'reference', $reference);
            $woocommerce->customer->set_shipping_city($city);
            $woocommerce->customer->set_shipping_state($state);
            $woocommerce->customer->set_shipping_address($address);
            $woocommerce->customer->set_shipping_postcode($zipcode);
            $woocommerce->customer->set_shipping_country('Brasil');

        } else {
            update_user_meta( $new_user_id, 'tipo', $_POST['tipo']);
            update_user_meta( $new_user_id, 'cpf', $_POST['CPF']);
            update_user_meta( $new_user_id, 'rg', $_POST['RG']);
            update_user_meta( $new_user_id, 'nascimento', $_POST['nascimento']);
            update_user_meta( $new_user_id, 'sexo', $_POST['sexo']);
            update_user_meta( $new_user_id, 'billing_phone', $_POST['telefone']);
            update_user_meta( $new_user_id, 'celular', $_POST['celular']);

            $response = array(
                'success' => true,
                'id' => $new_user_id
            );
        }
    } else {
        $response = $error;
    }

    print_r(json_encode($response));
    die();

}

add_action('wp_ajax_register', 'register');
add_action('wp_ajax_nopriv_register', 'register');


function shippingCalculate($cep = '', $product_id = 0)
{
    include __DIR__.'/../../plugins/woocommerce-correios/includes/class-wc-correios-webservice.php';

    global $woocommerce;
    $shipping_methods = array('40010' => 'SEDEX', '41106' => 'PAC');

    $items = WC()->cart->get_cart();
    foreach ($items as $item) {
        $frete_gratis = get_post_meta($item['product_id'], 'frete_gratis', true);
        if ($frete_gratis) {
            $shipping_cost = 0;
        }
    }

    $shipping = new WC_Correios_Webservice();
    if ($product_id > 0) {
        $shipping->set_width( get_post_meta($product_id, '_width', true) );
        $shipping->set_height( get_post_meta($product_id, '_height', true) );
        $shipping->set_length( get_post_meta($product_id, '_length', true) );
        $shipping->set_weight( get_post_meta($product_id, '_weight', true) );
    } else {
        $packages = $woocommerce->cart->get_shipping_packages();
        $woocommerce->cart->calculate_totals();
        $shipping->set_package($packages[0]);
    }

    $shipping->set_service(array(40010, 41106));
    $shipping->set_origin_postcode('04534-011');
    $shipping->set_destination_postcode($cep);
    $pack = $shipping->get_shipping();
    $options = array();
    foreach ($pack as $p) {
        $code = intval($p->Codigo);
        $p->metodo = $shipping_methods[$code];
        if (isset($shipping_cost) && $shipping_cost == 0) {
            $p->Valor = 0;
        } else if ($product_id == 0 && $p->metodo == 'PAC' && $woocommerce->cart->cart_contents_total > 149) {
            $p->Valor = 0;
        }
        $options[] = $p;
    }

    return json_encode($options);
}


function checkoutCalculate()
{
    $cep = $_POST['cep'];
    echo shippingCalculate($cep);
    die();
}

add_action('wp_ajax_checkoutCalculate', 'checkoutCalculate');
add_action('wp_ajax_nopriv_checkoutCalculate', 'checkoutCalculate');


function shippingProduct()
{
    echo shippingCalculate($_POST['zipcode'], $_POST['product_id']);
    die();
}

add_action('wp_ajax_shippingProduct', 'shippingProduct');
add_action('wp_ajax_nopriv_shippingProduct', 'shippingProduct');


function createOrder()
{
    global $woocommerce;

    print_r($woocommerce->checkout->process_checkout());
    // print_r($woocommerce->checkout->create_order());
    setcookie('address-checkout', '');
    die();
}

add_action('wp_ajax_createOrder', 'createOrder');
add_action('wp_ajax_nopriv_createOrder', 'createOrder');


function rating()
{
    global $wpdb;
    $id = $_POST['id'];
    $rating = $_POST['rating'];

    $results = $wpdb->query('INSERT INTO product_rating(post_id, rating) VALUES('.$id.', '.$rating.')');

}

add_action('wp_ajax_rating', 'rating');
add_action('wp_ajax_nopriv_rating', 'rating');


function filterProducts()
{
    $query = new WP_Query( array( 'cat' => $_POST['categories'] ) );
    
    die();
}

add_action('wp_ajax_filterProducts', 'filterProducts');
add_action('wp_ajax_nopriv_filterProducts', 'filterProducts');


function custom_woocommerce_correios_shipping_methods( $rates, $package ) {
    $items = WC()->cart->get_cart();
    foreach ($items as $item) {
        $frete_gratis = get_post_meta($item['product_id'], 'frete_gratis', true);
        if ($frete_gratis) {
            $shipping_cost = 0;
        }
    }


    if ( isset( WC()->cart->subtotal ) ) {
        foreach ( $rates as $key => $rate ) {
            if (isset($shipping_cost) && $shipping_cost == 0) {
                $rates[ $key ]['cost'] = 0;
            } 

            if ( 'PAC' == $rate['id'] && WC()->cart->cart_contents_total > 149 ) {
                $rates[ $key ]['cost'] = 0;
            }
        }
    }

    return $rates;
}

add_filter( 'woocommerce_correios_shipping_methods', 'custom_woocommerce_correios_shipping_methods', 10, 2 );


function searchProducts()
{
    global $wpdb;

    
    
    // $search = $_POST['search'];
    // $items  = array();
    // $results = $wpdb->get_results('SELECT * FROM wp_posts WHERE post_title LIKE "%'.$search.'%"');
    // foreach ($results as $r) {
    //     $brand = $wpdb->get_results('SELECT meta_value FROM wp_postmeta WHERE post_id = '.$r->ID.' AND (meta_key = "marca")');
    //     $brand_name = $wpdb->get_results('SELECT post_title FROM wp_posts WHERE ID = '.$brand[0]->meta_value);
    //     $price = $wpdb->get_results('SELECT meta_value FROM wp_postmeta WHERE post_id = '.$r->ID.' AND (meta_key = "_sale_price")');

    //     $items[] = array('title' => $r->post_title, 'brand' => $brand_name[0]->post_title, 'price' => number_format($price[0]->meta_value, 2, ',', '.'));
    // }

    // echo json_encode($items);
    die();
}

add_action('wp_ajax_searchProducts', 'searchProducts');
add_action('wp_ajax_nopriv_searchProducts', 'searchProducts');