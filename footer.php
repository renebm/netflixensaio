<footer role="footer">
	<div class="help-and-support">	
		<div class="center-content">
			<h1 class="title-rulers">CENTRAL DE ATENDIMENTO</h1>
		</div>
		<div class="information">
			<div class="generic-support column">
				<div class="blob">
					<span>AJUDA E SUPORTE</span>
				</div>
				<ul class="help">
					<li><a href="<?php echo get_bloginfo('url');?>/trocas-e-devolucoes">Trocas e Devoluções</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/politica-de-reembolso">Política de Reembolso</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/como-comprar">Como Comprar</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/entrega-e-frete">Entrega e Frete</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/formas-de-pagamento">Formas de Pagamento</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/seguranca-e-privacidade">Segurança e Privacidade</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/perguntas-frequentes">Perguntas Frequentes</a></li>
				</ul>
			</div>
			<div class="contact column">
				<div class="blob">
					<span>S.A.C</span>
				</div>
				<ul class="contact-segment">
					<li>
						<p class="segment">SAC</p>
						<span class="numbers"> 11 3683.0306</span>
					</li>
					<li>
						<p class="segment">WHATSAPP</p>
						<span class="numbers">11 97225.3257</span>
					</li>
					<li>
						<p class="segment">E-MAIL</p>
						<span>contato@nutracorpore.com.br</span>
					</li>
					<li>
						<p class="segment">HORÁRIO DE ATENDIMENTO</p>
						<span>Segunda a Quinta das 9h às 19h</span>
						<span>Sexta até as 18h</span>
						<span>Sábado das 9h às 13h</span>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="darken">
		<div class="center-content">	
			<div class="logo">
				<img src="<?php echo get_bloginfo('template_url');?>/images/common/logo.png" alt="Logotipo NutraCorpore">
			</div>
			<div class="institutional">
				<h1 class="category-title red">INSTITUCIONAL</h1>
				<ul>
					<li><a href="">HOME</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/quem-somos">QUEM SOMOS</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/cadastro">CADASTRE-SE</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/fale-conosco">FALE CONOSCO</a></li>
				</ul>
				<h2 class="red">SAC:<span>11 3683.0306</span></h2>
				<h2 class="red">WHATSAPP<span>11 97225.3257</span></h2>
			</div>
			<div class="profile">
				<h1 class="category-title red">PERFIL</h1>
				<ul>
					<li><a href="<?php echo get_bloginfo('url');?>/minha-conta">MINHA CONTA</a></li>
					<li><a href="<?php echo get_bloginfo('url');?>/meus-pedidos">MEUS PEDIDOS</a></li>
				</ul>
			</div>
			<div class="articles">
				<h1 class="category-title red">ARTIGOS</h1>
				<ul>
					<li><a href="<?php echo get_bloginfo('url').'/materias'; ?>">MATÉRIAS</a></li>
					<li><a href="<?php echo get_bloginfo('url').'/videos'; ?>">VÍDEOS</a></li>
					<li><a href="<?php echo get_bloginfo('url').'/ensaios'; ?>">ENSAIOS</a></li>
					<li><a href="<?php echo get_bloginfo('url').'/receitas'; ?>">RECEITAS</a></li>
					<li><a href="<?php echo get_bloginfo('url').'/eventos'; ?>">EVENTOS</a></li>
					<li><a href="<?php echo get_bloginfo('url').'/atletas'; ?>">ATLETAS</a></li>
				</ul>
			</div>
			<div class="product-types">
				<h1 class="category-title red">PRODUTOS</h1>
				<ul>
					<li><a href="">MASSA MUSCULAR</a></li>
					<li><a href="">ENERGIA E RESISTÊNCIA</a></li>
					<li><a href="">DEFINIÇÃO MUSCULAR</a></li>
					<li><a href="">EMAGRECIMENTO</a></li>
					<li><a href="">ACESSÓRIOS ESPORTIVOS</a></li>
					<li><a href="<?php echo get_bloginfo('url').'/marcas'; ?>">MARCAS</a></li>
					<li><a href="">KITS PROMOCIONAIS</a></li>
				</ul>
				<h2 class="red">SEGURANÇA</h2>
				<img src="<?php echo get_bloginfo('template_url');?>/images/common/comodo.png" alt="Logotipo Comodo certificado de segurança SSL">
			</div>
			<div class="social">
				<p>
					Novidades, Artigos, Dicas e Ofertas Exclusivas, cadastre-se.
				</p>
				<form class="newsletter">
					<input type="email" placeholder="Digite seu E-mail" class="newsletter-mail" name="newsletterMail">
					<button class="red-basic">ENVIAR</button>
				</form>
				<p>
					Nos acompanhe também nas Redes Sociais:
				</p>
				<a href="https://www.facebook.com/nutracorpore">	
					<i class="fa fa-facebook" aria-hidden="true"></i>
				</a>
				<a href="https://www.youtube.com/user/nutracorpore">	
					<i class="fa fa-youtube" aria-hidden="true"></i>
				</a>
				<h2 class="red">FORMAS DE PAGAMENTO</h2>
				<img src="<?php echo get_bloginfo('template_url');?>/images/common/cards.png" alt="VISA, MasterCard, American Express, Dinners, ELO e Boleto Bancário">
			</div>
		</div>
		<div class="copyright">
			<div class="center-content">	
				<p>Nutracorpore - CNPJ: 18.572.130/0001-64 - Rua Dona Primitiva Vianco, 901 - Centro - Osasco - São Paulo - CEP: 06010-000 | Copyright 2016 - Todos os direitos reservados</p>
				<a href="http://www.feeshop.com.br" target="_blank">
					<img src="<?php echo get_bloginfo('template_url');?>/images/common/feeshop.png" alt="Desenvolvido pela Agência Feeshop">
				</a>
			</div>
		</div>
	</div>
</footer>
<script src="https://use.fontawesome.com/dc89892f3f.js"></script>
<script src="<?php echo get_bloginfo('template_url'); ?>/node_modules/sweetalert/dist/sweetalert.min.js"></script>
<script src="<?php echo get_bloginfo('template_url'); ?>/dist/app.js"></script>
<script src="<?php echo get_bloginfo('template_url'); ?>/node_modules/jquery-zoom/jquery.zoom.min.js"></script>
<script src="<?php echo get_bloginfo('template_url'); ?>/src/js/jquery.fancybox.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.3/jquery.mask.min.js"></script>

<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html> 
