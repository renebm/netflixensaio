<!DOCTYPE html>
<!--[if IE]><html lang="pt-br" class="lt-ie9 lt-ie8"><![endif]-->
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title><?php echo get_bloginfo('site_name');?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="format-detection" content="telephone=no">
	<meta name="description" content="<?php echo get_bloginfo('site_description');?>">
	<meta name="language" content="pt-br">
	<meta name="author" content="Agência Feeshop">
	<meta property="og:locale" content="pt_BR">
	<meta property="og:url" content="">
	<meta property="og:title" content="">
	<meta property="og:site_name" content="">
	<meta property="og:description" content="">
	<meta property="og:image" content="">
	<meta property="og:image:type" content="">
	<meta property="og:image:width" content=""> 
	<meta property="og:image:height" content=""> 
	<meta property="og:type" content="website">
	<meta name="language" content="pt-br" />
	<link href="https://fonts.googleapis.com/css?family=Asap:400,400i,700" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_url');?>/src/main.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url');?>/node_modules/sweetalert/dist/sweetalert.css">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_bloginfo('template_url');?>/images/favicon/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="<?php echo get_bloginfo('template_url');?>/images/favicon/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo get_bloginfo('template_url');?>/images/favicon/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="<?php echo get_bloginfo('template_url');?>/images/favicon/manifest.json">
	<link rel="mask-icon" href="<?php echo get_bloginfo('template_url');?>/images/favicon/safari-pinned-tab.svg" color="#5bbad5">
	<link rel="shortcut icon" href="<?php echo get_bloginfo('template_url');?>/images/favicon/favicon.ico">
	<meta name="msapplication-config" content="<?php echo get_bloginfo('template_url');?>/images/favicon/browserconfig.xml">
	<meta name="theme-color" content="#ffffff">
</head>
<?php
global $current_user;
if (isset($_GET['logout'])) {
	setcookie('address-checkout', '');
	wp_logout();
	header('location: '.get_bloginfo('url'));
}

	// $info = array();
	// $info['user_login'] = 'gdgfd';
	// $info['user_password'] = '123456';
	// $info['remember'] = true;

	// $user_signon = wp_signon( $info, false );
	// echo isset($user_signon->errors);
	// print_r(json_encode($user_signon));
	// die();
?>
<body>
	<div class="oldiewarning" aria-hidden="true" hidden="true">
		<a href="https://www.google.com.br/chrome/browser/desktop/" aria-hidden="true">
			POR FAVOR, CLIQUE AQUI E ATUALIZE SEU NAVEGADOR PARA ACESSAR.
		</a>
	</div>
	<div class="loader-overlay">
		<div class="loader">
			<span class="loader-inner"></span>
		</div>
	</div>
	<header>
		<nav class="mobile">
			<ul class="upper">
				<li class="go-back"><i class="fa fa-angle-left" aria-hidden="true"></i><button class="return">VOLTAR</button></li>
				<li><a href="">HOME <i aria-hidden="true" class="fa fa-chevron-right"></i></a></li>
				<li>
					<a>PRODUTOS<i aria-hidden="true" class="fa fa-chevron-right"></i></a>
					<ul class="submenu">
						<li><a href="">MASSA MUSCULAR</a></li>
						<li><a href="">ENERGIA E RESISTÊNCIA</a></li>
						<li><a href="">DEFINIÇÃO MUSCULAR</a></li>
						<li><a href="">EMAGRECIMENTO</a></li>
						<li><a href="">ACESSÓRIOS ESPORTIVOS</a></li>
						<li><a href="">MARCAS</a></li>
						<li><a href="">KITS PROMOCIONAIS</a></li>
					</ul>
				</li>
				<li><a href="">AJUDA E SUPORTE <i aria-hidden="true" class="fa fa-chevron-right"></i></a></li>
				<li>
					<a>INSTITUCIONAL <i aria-hidden="true" class="fa fa-chevron-right"></i></a>
					<ul class="submenu">
						<li><a href="<?php echo get_bloginfo('url'); ?>/quem-somos">QUEM SOMOS</a></li>
						<li><a href="<?php echo get_bloginfo('url'); ?>/cadastro">CADASTRE-SE</a></li>
						<li><a href="<?php echo get_bloginfo('url'); ?>/fale-conosco">FALE CONOSCO</a></li>
					</ul>
				</li>
				<li><a href="">MINHA CONTA <i aria-hidden="true" class="fa fa-chevron-right"></i></a></li>
				<li>
					<a>ARTIGOS <i aria-hidden="true" class="fa fa-chevron-right"></i></a>
					<ul class="submenu">
						<li><a href="">MATÉRIAS</a></li>
						<li><a href="">VÍDEOS</a></li>
						<li><a href="">ENSAIOS</a></li>
						<li><a href="">RECEITAS</a></li>
						<li><a href="">EVENTOS</a></li>
						<li><a href="">ATLETAS</a></li>
					</ul>
				</li>
			</ul>
			<div class="contact-information">
				<div>	
					<p>SAC</p>
					<span>11 3683.0306</span>
					<small>seg à sex - 10h às 18h</small>
				</div>
				<div>
					<p>WHATSAPP</p>
					<span>11 93498.5569</span>
				</div>
			</div>	
		</nav>
		<nav>
			<div class="center-content">
				<button class="hamburger" aria-role="Menu" aria-controls="Navigation">
					<div class="inner">	
						<span></span>
						<span></span>
						<span></span>
					</div>
				</button>
				<div class="information-and-login">
					<div class="login">
						<p style="display: block;"><i class="fa fa-user"></i>&nbsp;<span>Bem-Vindo 
							<?php echo (is_user_logged_in()) ? $current_user->user_nicename : ''; ?> | </span>
							<?php
							if (! is_user_logged_in()) :
								?>
							<a href="<?php echo get_bloginfo('url');?>/login">ENTRAR  </a>ou<a href="<?php echo get_bloginfo('url');?>/cadastro" class="red">  CADASTRE-SE</a></p>
							<?php
							else :
								?>
							<a href="<?php echo get_bloginfo('url');?>/?logout=sair">SAIR</a>
							<?php
							endif;
							?>
						</div>
						<div class="search">
							<form action="">	
								<input type="text" placeholder="O que você está procurando?">
								<button type="submit"><i class="fa fa-search"></i></button>
							</form>
							<div class="dropdown white-box">
								<ul class="entries" id="search_results">
									<li>
										<a href="">
											<div class="picture"><img src="https://placeholdit.imgix.net/~text?txtsize=33&amp;txt=350%C3%97150&amp;w=79&amp;h=87" alt=""></div>
											<span>Whey Gold <br> Optimum Nutrition</span>
											<span class="price">R$ 450,00</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<a href="<?php echo get_bloginfo('url'); ?>">
						<div class="logo">	
						</div>
					</a>
					<div class="orders-account-cart">	
						<ul>
							<li>
								<a href="<?php echo get_bloginfo('url'); ?>/meus-pedidos">
									<span class="lines" aria-hidden="true">
										<span aria-hidden="true"></span>
										<span aria-hidden="true"></span>
										<span aria-hidden="true"></span>
										<span aria-hidden="true"></span>
									</span>
									MEUS PEDIDOS
									&nbsp; 
								</a>
								|
							</li>
							<li>
								<a href="<?php echo get_bloginfo('url'); ?>/minha-conta">
									&nbsp;
									<i class="fa fa-user"></i>
									MINHA CONTA 
									&nbsp;
								</a>
								|
							</li>
							<li>
								&nbsp;
								REDES SOCIAIS:
								&nbsp;
								<a href="https://www.facebook.com/nutracorpore" target="_blank">
									<i class="fa fa-facebook"></i>
								</a>
								<a href="https://www.youtube.com/user/nutracorpore" target="_blank">
									<i class="fa fa-youtube"></i>
								</a>
							</li>
						</ul>
						<div class="cart">
							<i class="fa fa-shopping-cart" aria-hidden="true"></i>
							<?php
							global $woocommerce;
							$items = $woocommerce->cart->get_cart();
							$woocommerce->cart->calculate_totals();
							?>
							<small><?php echo count($items); ?></small>
							<div class="dropdown white-box">
								<ul class="entries">
									<?php
									if(count($items) > 0){
										$frete_produto = array();
										foreach($items as $item => $values) {
											$frete_produto[] = $values['product_id'];
											$_product = $values['data']->post;
											$price = get_post_meta($values['product_id'] , '_price', true);
											?>
											<li>
												<a href="<?php echo $values['post']->guid; ?>">
													<div class="picture">
														<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $values['product_id'], 'thumbnail' ) ); ?>">
													</div>
													<span><?php echo $values['data']->post->post_title; ?> <br> <?php //echo get_field('marca'); ?></span>
													<span class="price">R$ <?php echo number_format($price, 2, ',', '.'); ?></span>
												</a>
											</li>
											<?php
										}
									} else {
										echo '<p>Carrinho Vazio</p>';
									}
									?>
								</ul>
								<div class="overall">
									<p>
										<span><?php echo count($items); ?> produto(s) no carrinho.</span>
										<span>Total: <?php echo $woocommerce->cart->get_cart_total(); ?></span>
									</p>
									<a href="<?php echo get_bloginfo('url'); ?>/carrinho">
										IR PARA O CARRINHO
									</a>
								</div>
							</div>
						</div>
						<div class="customer-support">
							<p>CENTRAL DE ATENDIMENTO</p>
							<!-- <img src="images/icons/talk-balloon-small.svg" aria-hidden="true"> -->
							<i class="fa fa-comments-o" aria-hidden="true"></i>
							<ul class="contact-widget white-box" tabindex="-1">
								<li>
									<h1>TELEFONE</h1>
									<h2>11 3498.5569</h2>
									<small>seg à sex - 10h às 18h</small>
								</li>
								<li>
									<h1>WHATSAPP</h1>
									<h2>11 93498.5569</h2>
								</li>
								<li>
									<h1>E-MAIL</h1>
									<h2>contato@nutracorpore.com.br</h2>
								</li>
								<li>
									<span>Dúvidas ?<a href="">FALE CONOSCO</a></span>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</nav>
		</header>