<?php 
	get_header();
?>
<section class="highlights">
	<div class="full-bg">
		<?php
		$args = array(
			'post_type' => 'Banner',
			'order' => 'desc'
			);
		$loop = new WP_Query( $args );
		if ( $loop->have_posts() ) :
			while ( $loop->have_posts() ) : $loop->the_post();
		if (get_field('tipo_de_banner') == 'hero') : 
			?>
		<div class="slide" 
		style="background-image: url(<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) );?>);"></div>
		<?php
		endif;
		endwhile;
		endif;
		?>
	</div>
	<div class="center-content">	
		<div class="forepart">
			<div>
				<span>10% de DESCONTO</span>
				<i class="fa fa-tag" aria-hidden="true"></i>
				<span>Para compras à vista.</span>
			</div>
			<div>
				<span>FRETE GRÁTIS</span>
				<i class="fa fa-truck" aria-hidden="true"></i>
				<span>Para compras acima de R$ 149,00.</span>
			</div>
			<div>
				<span>TEM UMA DÚVIDA NUTRICIONAL?</span>
				<i class="fa fa-comments" aria-hidden="true"></i>
				<span>Fale com nossa nutricionista <a class="red" href="<?php echo site_url('/fale-conosco/') ?>">aqui.</a></span>
			</div>
		</div>
		<div class="section-picker">
			<div class="option active" data-picker="objectives">
				<div class="heading">
					<button class="submenu-picker" data-picker="objectives" data-url="/teste">	
						<h1>OBJETIVOS</h1>
					</button>
				</div>
				<ul class="option-submenu objectives">
					<li class="overview blue">
						<div class="background-form" aria-hidden="true"></div>
						<div class="highlight muscle"></div>
						<span class="subtitle">
							<h2>GANHAR MASSA MUSCULAR</h2>
						</span>
					</li>
					<li class="overview red">
						<div class="background-form" aria-hidden="true"></div>
						<div class="highlight thinning"></div>
						<span class="subtitle">
							<h2>EMAGRECIMENTO & DEFINIÇÃO</h2>
						</span>
					</li>
					<li class="overview yellow">
						<div class="background-form" aria-hidden="true"></div>
						<div class="highlight energy"></div>
						<span class="subtitle">
							<h2>MAIS ENERGIA E RESISTÊNCIA</h2>
						</span>
					</li>
					<li class="overview green">
						<div class="background-form" aria-hidden="true"></div>
						<div class="highlight health"></div>
						<span class="subtitle">
							<h2>SAÚDE E BEM-ESTAR</h2>
						</span>
					</li>
				</ul>
			</div>
			<div class="option" data-picker="categories">
				<div class="heading">
					<button class="submenu-picker" data-picker="categories">	
						<h1>CATEGORIAS</h1>
					</button>
				</div>
				<ul class="option-submenu categories five">
					<?php
						$sql  = 'SELECT wt.* ';
						$sql .= 'FROM wp_terms AS wt ';
						$sql .= 'INNER JOIN wp_term_taxonomy AS wtt ON wtt.term_id = wt.term_id ';
						$sql .= 'WHERE ';
						$sql .= 'wtt.parent = 0 AND wtt.taxonomy = "product_cat" LIMIT 5';
						
						$results = $wpdb->get_results($sql);
						foreach ($results as $r) :
					?>
						<li>
							<ul>
								<li><h2><?php echo $r->name; ?></h2></li>
								<?php
									$subsql  = 'SELECT wt.* ';
									$subsql .= 'FROM wp_terms AS wt ';
									$subsql .= 'INNER JOIN wp_term_taxonomy AS wtt ON wtt.term_id = wt.term_id ';
									$subsql .= 'WHERE ';
									$subsql .= 'wtt.parent = '.$r->term_id.' AND wtt.taxonomy = "product_cat" LIMIT 15';

									$sub_results = $wpdb->get_results($subsql);

									foreach ( $sub_results as $sub ) :
								?>
										<li><a href="<?php echo get_bloginfo('url').'/categoria/'.$sub->slug; ?>"><?php echo $sub->name; ?></a></li>
								<?php
									endforeach;

									if (count($sub_results) < 15) {
										$complete = 15 - count($sub_results);
										for ($i=1; $i<=$complete; $i++) {
								?>
											<li></li>
								<?php
										}
									}
								?>
							</ul>
						</li>
					<?php
						endforeach;
					?>
				</ul>
			</div>
			<div class="option" data-picker="brands">
				<div class="heading">
					<button class="submenu-picker" data-picker="brands">	
						<h1>MARCAS</h1>
					</button>
				</div>
				<ul class="option-submenu brands five">
					<?php
						$args = array(
							'post_type' => 'marca',
							'posts_per_page' => 15
						);
						$loop = new WP_Query( $args );
						$cnt = 0;
						if ( $loop->have_posts() ) :
							while ( $loop->have_posts() ) : $loop->the_post();
								if ($cnt == 0) :
					?>
								<li>
									<ul>
								<?php
								endif;	
								$post_data = get_post(get_the_ID());
								$post_name = $post_data->post_name;
								?>
										<li>
											<a href="<?php echo get_bloginfo('url') ;?>/categoria/<?php echo $post_name; ?>">
												<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) ); ?>" alt="">
											</a>
										</li>
					<?php
								$cnt++;
								if ($cnt == 3) :
									$cnt = 0;
					?>
									</ul>
								</li>
					<?php
								endif;
							endwhile;
						endif;
					?>
					<li class="full"><a href="<?php echo site_url('/marcas/'); ?>">VER MAIS</a></li>
				</ul>
			</div>
			<div class="option" data-picker="sales">
				<div class="heading">
					<button class="submenu-picker" data-picker="sales">	
						<h1>KITS PROMOCIONAIS</h1>
					</button>
				</div>
				<div class="option-submenu sales">
					<section class="grid-products greyed five">
						<?php
							$args = array(
								'post_type' => 'product',
								'category_name' => 'kits-promocionais'
							);

							$payment_options   = get_option('woocommerce_cielo_credit_settings');
							$payment_discounts = get_option('woocommerce_payment_discounts');
							$percent 			= str_replace('%', '', $payment_discounts['boleto']);

							$loop = new WP_Query( $args );
							if ( $loop->have_posts() ) :
								while ( $loop->have_posts() ) : $loop->the_post();
									$installments = number_format(($product->get_price() / $payment_options['installments']), 2, ',', '.');
							?>
							<div class="product">
								<figure>
									<?php 
									if (get_field('frete_gratis')) { 
										?>
										<span class="generic-stripe orange">FRETE GRÁTIS</span>
										<?php
									}
									?>
									<?php 
									if (get_field('lancamento')) { 
									?>
										<span class="generic-stripe green">LANÇAMENTO</span>
									<?php
									}
									?>
									<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) );?>" alt="">
									<div class="overlay">
										<a href="<?php echo get_the_permalink(); ?>" class="product-detail-page 
										<?php echo ($product->product_type == 'variable') ? 'no-add' : '';?>">
											<i class="fa fa-search" aria-hidden="true"></i>
										</a>
										<?php
										if ($product->product_type != 'variable') :
										?>
										<button class="add-cart" data-id="<?php echo get_the_ID();?>">
											<i class="fa fa-shopping-cart" aria-hidden="true"></i>
										</button>
										<?php
										endif;
										?>
									</div>
									<figcaption class="name">
										<a href="<?php echo get_the_permalink(); ?>">
											<?php echo get_the_title(); ?>
										</a>
									</figcaption>
								</figure>
								<?php
									$full_price = get_field('_regular_price');
									$price 		= get_field('_sale_price');
									
									if ($full_price != $price && !empty($price)) :
								?>
										<p class="discounted-difference">De: <span class="striked">R$<?php echo number_format($full_price, 2, '.', ','); ?></span>  Por: <b>R$<?php echo number_format($price, 2, '.', ','); ?></b><span>ou em até <?php echo $payment_options['installments']; ?>x de R$ <?php echo $installments; ?></span></p>
								<?php
									else :
								?>
										<p class="full-price">
											R$ <?php echo number_format($product->get_price(), 2, ',', '.'); ?>
											<span class="installments">em até <?php echo $payment_options['installments']; ?>x de R$ <?php echo $installments; ?></span>
										</p>
								<?php
									endif;
								?>
								<p class="discount-price">
									<?php
									$price = $product->get_price() - ($product->get_price() * ($percent / 100));
									$price = number_format($price, 2, '.', ',');
									$price = explode('.', $price);
									?>
									<?php echo $price[0]; ?>,<span class="cents"><?php echo (! isset($price[1])) ? '00' : $price[1]; ?></span>
								</p>
								<small>no boleto ou depósito</small>
							</div>
							<?php
							endwhile;
							endif;
						?>
					</section>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="division">
	<div class="center-content">
		<div class="texty">	
			<h1>NEWSLETTER</h1>
			<h2>Novidades, Artigos, Dicas e Ofertas Exclusivas, cadastre-se.</h2>
		</div>	
		<form action="" class="newsletter">
			<input type="email" placeholder="Digite seu E-mail" class="newsletter-mail" name="newsletterMail">
			<button class="red-basic">ENVIAR</button>
		</form>
	</div>
</div>
<section class="top-sellers">
	<div class="center-content">
		<div class="starred">	
			<i class="fa fa-star" aria-hidden="true"></i>
			<i class="fa fa-star huge" aria-hidden="true"></i>
			<i class="fa fa-star" aria-hidden="true"></i>
		</div>
		<h1 class="title-rulers">MAIS VENDIDOS</h1>
		<div class="grid-products five">
			<?php
			$args = array(
				'post_type' => 'product',
				'posts_per_page' => 5
			);

			$payment_options   = get_option('woocommerce_cielo_credit_settings');
			$payment_discounts = get_option('woocommerce_payment_discounts');
			$percent 			= str_replace('%', '', $payment_discounts['boleto']);

			$loop = new WP_Query( $args );
			if ( $loop->have_posts() ) :
				while ( $loop->have_posts() ) : $loop->the_post();
					$installments = number_format(($product->get_price() / $payment_options['installments']), 2, ',', '.');
			?>
			<div class="product">
				<figure>
					<?php 
					if (get_field('frete_gratis')) { 
						?>
						<span class="generic-stripe orange">FRETE GRÁTIS</span>
						<?php
					}
					?>
					<?php 
					if (get_field('lancamento')) { 
					?>
						<span class="generic-stripe green">LANÇAMENTO</span>
					<?php
					}
					?>
					<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( get_the_ID(), 'thumbnail' ) );?>" alt="">
					<div class="overlay">
						<a href="<?php echo get_the_permalink(); ?>" class="product-detail-page 
						<?php echo ($product->product_type == 'variable') ? 'no-add' : '';?>">
							<i class="fa fa-search" aria-hidden="true"></i>
						</a>
						<?php
						if ($product->product_type != 'variable') :
						?>
						<button class="add-cart" data-id="<?php echo get_the_ID();?>">
							<i class="fa fa-shopping-cart" aria-hidden="true"></i>
						</button>
						<?php
						endif;
						?>
					</div>
					<figcaption class="name">
						<a href="<?php echo get_the_permalink(); ?>">
							<?php echo get_the_title(); ?>
						</a>
					</figcaption>
				</figure>
				<?php
					$full_price = get_field('_regular_price');
					$price 		= get_field('_sale_price');
					
					if ($full_price != $price && !empty($price)) :
				?>
						<p class="discounted-difference">De: <span class="striked">R$<?php echo number_format($full_price, 2, '.', ','); ?></span>  Por: <b>R$<?php echo number_format($price, 2, '.', ','); ?></b><span>ou em até <?php echo $payment_options['installments']; ?>x de R$ <?php echo $installments; ?></span></p>
				<?php
					else :
				?>
						<p class="full-price">
							R$ <?php echo number_format($product->get_price(), 2, ',', '.'); ?>
							<span class="installments">em até <?php echo $payment_options['installments']; ?>x de R$ <?php echo $installments; ?></span>
						</p>
				<?php
					endif;
				?>
				<p class="discount-price">
					<?php
					$price = $product->get_price() - ($product->get_price() * ($percent / 100));
					$price = number_format($price, 2, '.', ',');
					$price = explode('.', $price);
					?>
					<?php echo $price[0]; ?>,<span class="cents"><?php echo (! isset($price[1])) ? '00' : $price[1]; ?></span>
				</p>
				<small>no boleto ou depósito</small>
			</div>
			<?php
			endwhile;
			endif;
			?>
		</div>
		<button class="see-more">VER MAIS</button>
	</div>
</section>
<div class="division">
	<div class="center-content">	
		<h1>NUTRACORPORE TV</h1>
		<a href="https://www.youtube.com/user/nutracorpore" target="_blank">	
			<div class="youtube"></div>
		</a>
		<h2>Inscreva-se e acesse nosso conteúdo exclusivo em HD.</h2>
	</div>
</div>
<section class="offers">
	<div class="center-content">	
		<h1 class="title-rulers">OFERTAS</h1>	
		<div class="grid-products greyed five">

			<div class="product">
				<figure>
					<span class="generic-stripe orange">FRETE GRÁTIS</span>
					<span class="generic-stripe green">LANÇAMENTO</span>
					<img src="https://placeholdit.imgix.net/~text?txtsize=19&txt=200%C3%97182&w=200&h=182" alt="">
					<div class="overlay">
						<button class="product-detail-page">
							<i class="fa fa-search" aria-hidden="true"></i>
						</button>
						<button class="add-cart">
							<i class="fa fa-shopping-cart" aria-hidden="true"></i>
						</button>
					</div>
					<figcaption class="name">
						<a href="">
							Whey Gold Optimum Nutrition
						</a>
					</figcaption>
				</figure>
				<p class="discounted-difference">De: <span class="striked">R$317,42</span>  Por: <b>R$269,00</b><span>ou em até 6x de R$ 46,67</span></p>
				<p class="discount-price">
					450,<span class="cents">00</span>
				</p>
				<small>no boleto ou depósito</small>
			</div>

			<div class="product">
				<figure>
					<img src="https://placeholdit.imgix.net/~text?txtsize=19&txt=200%C3%97182&w=200&h=182" alt="">
					<div class="overlay">
						<button class="product-detail-page">
							<i class="fa fa-search" aria-hidden="true"></i>
						</button>
						<button class="add-cart">
							<i class="fa fa-shopping-cart" aria-hidden="true"></i>
						</button>
					</div>
					<figcaption class="name">
						
					</figcaption>

				</figure>
				<p class="full-price">R$ 280,00<span class="installments">em até 6x de R$ 46,67</span></p>
				<p class="discount-price">
					450,<span class="cents">00</span>
				</p>
				<small>no boleto ou depósito</small>
			</div>
		</div><!--ENDGRID -->
	</div><!--ENDCENTER -->
</section>
<section class="blog">
	<div class="center-content">
		<h1 class="title-rulers hidden">
			<span class="mobile-only">CONTEÚDO EXCLUSIVO</span>	
			<img src="<?php echo get_bloginfo('template_url');?>/images/common/exclusive.jpg" alt="Conteúdo Exclusivo: Vídeos, Ensaios, Receitas, Artigos sobre suplementação, Musculação, saúde e muito mais" class="exclusive">
		</h1>	
		<ul class="topics">
			<li><a href="<?php echo get_bloginfo('url').'/materias'; ?>">MATÉRIAS</a></li>
			<li><a href="<?php echo get_bloginfo('url').'/videos'; ?>">VÍDEOS</a></li>
			<li><a href="<?php echo get_bloginfo('url').'/ensaios'; ?>">ENSAIOS</a></li>
			<li><a href="<?php echo get_bloginfo('url').'/receitas'; ?>">RECEITAS</a></li>
			<li><a href="<?php echo get_bloginfo('url').'/eventos'; ?>">EVENTOS</a></li>
			<li><a href="<?php echo get_bloginfo('url').'/atletas'; ?>">ATLETAS</a></li>
		</ul>
		<!-- <div class="feed">
			<p>
				<strong>
					PUBLICAÇÃO 1:
				</strong>
				commodi id nemo ducimus! Ad ea assumenda, id qui dignissimos quia iste soluta. Architecto cum quam ex, aliquid placeat.
				<a href="" class="red">
					Leia Mais
				</a>
			</p>
			<p>
				<strong>
					PUBLICAÇÃO 2:
				</strong>
				commodi id nemo ducimus! Ad ea assumenda, id qui dignissimos quia iste soluta. Architecto cum quam ex, aliquid placeat.
				<a href="" class="red">
					Leia Mais
				</a>
			</p>
		</div> -->
	</div>
</section>
<?php 
get_footer();
?>